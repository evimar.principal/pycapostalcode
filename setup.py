from setuptools import setup, find_packages
import sys, os

version = '0.3.4'

setup(name='pycapostalcode',
      version=version,
      description="Calgary and surrounding zones postal codes",
      long_description=open("README.txt").read() + '\n\n',
      classifiers=[], 
      keywords='Canada postal code Calgary Airdrie Chestermere',
      author='Evimar Principal',
      author_email='evimar.principal@gmail.com',
      url='https://gitlab.com/evimar.principal/pycapostalcode',
      download_url='https://gitlab.com/evimar.principal/pycapostalcode/tarball/0.3.4',	
      license='GPL',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )

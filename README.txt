Introduction
************

This is a fork of Scott Rockey pypostalcode that is a fork of Nathan Van Gheem's excellent pyzipcode package.  The zipcode database has been replaced with Calgary, Airdrie, Chestermere, Rocky View County and Redwood Meadows cities from Alberta Canada, and their postal codes. The general usage is the same.
        
Canadian postal codes are six characters with this format: A1A 1A1, where A is a letter and 1 is a digit, with a space separating the third and fourth characters. The first three digits are the Forward Sortation Area ©  (FSA), and the last three are the Local Delivery Unit (LDU). 

This module only uses the whole FSA + LDU designator for location, including the space char in the middle. 


To install:
------------------------------------
pip install pycapostalcode


Basic usage:
------------------------------------


	>>> from pycapostalcode import PostalCodeDatabase
	>>> pcdb = PostalCodeDatabase()
	>>> pc = 'T1X 0L3'
	>>> location = pcdb[pc]
	>>> location.postalcode
	u'T1X 0L3'
	>>> location.city
	u'Calgary'
	>>> location.province
	u'AB'


Know if a postal code is in the database:
-----------------------------------------
	>>> from pycapostalcode import PostalCodeDatabase
	>>> pcdb = PostalCodeDatabase()
	>>> pc = 'R3Z 0L1'
	>>> pcdb.exists(pc)
    	False



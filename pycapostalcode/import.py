
import sqlite3

import csv
try:
    from settings import db_location
except:
    from pyzipcode.settings import db_location

conn = sqlite3.connect(db_location)
c = conn.cursor()

c.execute("DROP TABLE IF EXISTS PostalCodes;")
c.execute("CREATE TABLE PostalCodes(code VARCHAR(7), city TEXT, province TEXT);")
c.execute("CREATE INDEX code_index ON PostalCodes(code);")

reader = csv.reader(open('ca_calgary_surroundings_postalcodes.csv', "r"))

    
for row in reader:
    code, city = row     #[:3]+row[0][4:]
    c.execute('INSERT INTO PostalCodes values("%s", "%s","AB")' % (code, city))
    
conn.commit()

# We can also close the cursor if we are done with it
c.close()

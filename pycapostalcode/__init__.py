from .settings import db_location
import sqlite3
import time

'''
(c) This data includes information copied with permission from Canada Post Corporation.
'''

class ConnectionManager(object):
    """
    Assumes a database that will work with cursor objects
    """
    
    def __init__(self):
        # test out the connection...
        conn = sqlite3.connect(db_location)
        conn.close()
 
           
    def query(self, sql):
        conn = None
        retry_count = 0
        while not conn and retry_count <= 10:
        # If there is trouble reading the file, retry for 10 attempts
        # then just give up...
            try:
                conn = sqlite3.connect(db_location)
            except sqlite3.OperationalError:
                retry_count += 1
                time.sleep(0.001)
        
        if not conn and retry_count > 10:
            raise sqlite3.OperationalError("Can't connect to sqlite database.")
                
        cursor = conn.cursor()
        cursor.execute(sql)
        res = cursor.fetchall()
        conn.close()
        return res


PC_QUERY = "SELECT * FROM PostalCodes WHERE code='%s'"
PC_FIND_QUERY = "SELECT * FROM PostalCodes WHERE city LIKE '%s' AND province LIKE '%s'"
CITIES_QUERY = "SELECT DISTINCT '', city, province FROM PostalCodes WHERE province LIKE '%s' ORDER BY 2"
PROVINCES_QUERY = "SELECT DISTINCT '', '', province FROM PostalCodes"


class PostalCode(object):


    def __init__(self, data):
        self.postalcode = data[0]
        self.city = data[1]
        self.province = data[2]


def format_result(postalcodes):
    if len(postalcodes) > 0:
        return [PostalCode(code) for code in postalcodes]
    else:
        return None


class PostalCodeNotFoundException(Exception):
    pass
    

class PostalCodeDatabase(object):
    

    def __init__(self, conn_manager=None):
        if conn_manager is None:
            conn_manager = ConnectionManager()
        self.conn_manager = conn_manager      
        
                    
    def find_postalcode(self, city=None, province=None):
        if city is None:
            city = "%"
        else:
            city = city.upper()
            
        if province is None:
            province = "%"
        else:
            province = province.upper()
            
        return format_result(self.conn_manager.query(PC_FIND_QUERY % (city, province)))
        
    
    def get_cities(self, province=None):
            
        if province is None:
            province = "%"
        else:
            province = province.upper()
            
        return format_result(self.conn_manager.query(CITIES_QUERY % (province)))
        

    def get_provinces(self):           
        return format_result(self.conn_manager.query(PROVINCES_QUERY))
        

    def get(self, pc):
        return format_result(self.conn_manager.query(PC_QUERY % pc))

        
    def exists(self, pc):
        return len(self.conn_manager.query(PC_QUERY % pc)) 
            
    def __getitem__(self, pc):
        pc = self.get(str(pc))
        if pc is None:
            raise IndexError("Couldn't find postal code")
        else:
            return pc[0]
